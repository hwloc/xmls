#!/usr/bin/node
/*
 * Copyright © 2019-2020 Inria.  All rights reserved.
 */

const fs = require('fs')
const xml2js = require('xml2js')
const parser = new xml2js.Parser({ attrkey: "ATTR" })
const exec = require('child_process').exec

process.argv.forEach(function (xml, index) {
    if(index > 1)
        parseXML(xml)
})

function parseXML(xml){
    /* Prepare tags that will be use to describe the topology, as object so it can be passed easily to functions */
    let tags = { 
        "architecture": null,
        "uname-architecture": null,
        "sub-architecture": null,
        "cpu-vendor": null,
        "cpu-family": null,
        "cpu-model": null,
        "nbcores": 0,
        "nbpackages": 0,
        "NUMA": 0,
        "chassis": null,
        "year": null,
        "operating-system": null,
        "version": null 
    }
    /* Optional tags */
    let infos = {
        "PU": 0,
        "Die" : 0
    }

    /* Convert .xml file into json file to run through it */
    let xml_string = fs.readFileSync(xml, "utf8")
    parser.parseString(xml_string, async function(error, result) {
        if(error === null) {
            const topology = result.topology.object[0].info
            const machine = result.topology.object[0].object

            /* Set non numerical infos, for instance : architecture, vendor, chassis.. */
            setInfoTags(topology, tags)
            setInfoTags(machine[0].info, tags)
            
            /* Parse the xml and increment every object it runs through, for instance : packages, cores, PU.. */
            deepCourse(machine, tags, infos)

            /* Set infos not explicitly written in the .xml file */
            let SMT = infos.PU / tags.nbcores
            if(SMT > 1)
                tags.SMT = SMT
            if(tags.MemorySideCache || infos.Die > 0)
                tags.version = 2.1
            else
                tags.version = 2
            if(tags.CUDA)
                tags.GPU = tags.CUDA
            else if(tags.OpenCL)
                tags.GPU = tags.OpenCL
            
            let fileName = await setName(tags, xml)
            setJSON(tags, fileName)
        }
        else {
            console.log("wrong file type")
        }
    });
}

function setInfoTags(object, tags){
    if(!Array.isArray(object))
        return
    for(let i = 0 ; i < object.length ; i++){
        switch(object[i]["ATTR"].name){
            case "CPUVendor":
                if(object[i]["ATTR"].value == "AuthenticAMD")
                    tags["cpu-vendor"] = "AMD"
                else if(object[i]["ATTR"].value == "GenuineIntel")
                    tags["cpu-vendor"] = "Intel"
                else
                    tags["cpu-vendor"] = object[i]["ATTR"].value
                break
            case "CPUModel":
                tags["cpu-model"] = object[i]["ATTR"].value
                break
            case "Architecture":
                tags["uname-architecture"] = object[i]["ATTR"].value
                if(object[i]["ATTR"].value == "x86_64")
                    tags["architecture"] = "x86"
                else if(object[i]["ATTR"].value == "aarch64")
                    tags["architecture"] = "arm"
                else if(object[i]["ATTR"].value.includes("pp"))
                    tags["architecture"] = "POWER"
                break
            case "OSName":
                tags["operating-system"] = object[i]["ATTR"].value
                break
            case "DMIChassisVendor":
                tags["chassis"] = object[i]["ATTR"].value + " " + tags["chassis"]
                break
            case "DMIProductName":
                tags["chassis"] = object[i]["ATTR"].value
                break
            case "DMIBIOSDate":
                tags["year"] = object[i]["ATTR"].value.substring(6)
        }
    }
}

function deepCourse(object, tags, infos){
    if(Array.isArray(object)){
        object.forEach(child => {
            deepCourse(child, tags, infos)
        })
    } else {
        if(object.object)
            deepCourse(object.object, tags, infos)
        switch(object["ATTR"].type){
            case "Core":
                tags.nbcores++
                break
            case "PU":
                infos.PU++
                break
            case "NUMANode":
                tags.NUMA++
                if(object["ATTR"]["subtype"] || (object.info && object.info[0]["ATTR"]["name"] == "DAXDevice"))
                    tags.HeterogeneousNUMA ? tags.HeterogeneousNUMA++ : tags.HeterogeneousNUMA = 1
                break
            case "Package":
                tags.nbpackages++
                break
            case "OSDev":
                if(object["ATTR"]["osdev_type"] == 3)
                    tags.InfiniBand ? tags.InfiniBand++ : tags.InfiniBand = 1
                else if(object["ATTR"]["subtype"] == "CUDA")
                    tags.CUDA ? tags.CUDA++ : tags.CUDA = 1
                else if(object["ATTR"]["subtype"] == "OpenCL")
                    tags.OpenCL ? tags.OpenCL++ : tags.OpenCL = 1
                else if(object["ATTR"]["name"].includes("dax"))
                    tags.DAX ? tags.DAX++ : tags.DAX = 1
                else if(object["ATTR"]["name"].includes("pmem"))
                    tags.PMEM ? tags.PMEM++ : tags.PMEM = 1
                break
            case "MemCache":
                tags.MemorySideCache ? tags.MemorySideCache++ : tags.MemorySideCache = 1
                break
            case "Die":
                infos.Die++
                break
        }
        /* TODO: if NUMA nodes attached to different depths/types, also set the HeterogeneousNUMA tag */
    }
}

async function setName(tags, xml){
    let name
    let cpuModel = tags["cpu-model"] ? tags["cpu-model"].split(" ") : ""
    let lstopoResult = await lstopo(xml)
    if(lstopoResult.error)
            return xml

    name = cpuModel[0] + "-" + cpuModel[1] + lstopoResult.result
    name += tags.InfiniBand ? ("+" + (tags.InfiniBand > 1 ? tags.InfiniBand : "") + "ib") : ""
    name += tags.CUDA ? ("+" + (tags.CUDA > 1 ? tags.CUDA : "") + "cuda") : ""

    return name
}

function setJSON(json, fileName){
    let rawdata = fs.readFileSync(__dirname + '/../tags.json')
    let tags = JSON.parse(rawdata)

    tags[fileName] = json
    fs.writeFileSync(__dirname + '/../tags.json', JSON.stringify(tags, null, 2))
}


async function lstopo(xml) {
    return new Promise(function (resolve, reject) {
      exec("lstopo -i " + xml + " --output-format synthetic --export-synthetic-flags 3", (err, result, stderr) => {
        if (err) {
            console.log("Something went wrong with lstopo, using filename provided as parameter to write json. See the error above : ")
            console.log(stderr)
            resolve({ error: true, stderr})
        } else {
            result = parseLstopo(result)
            resolve({ result, stderr })
        }
      })
    })
}

function parseLstopo(lstopoResult){
    let result = "-"
    lstopoResult = lstopoResult.replace(/\r?\n|\r/g,"").split(" ")

    // TODO ignore caches when cores are identical below it, just report cores
    // e.g. 2pa2ca1co becomes 2pa2co
    for(let i = 0 ; i < lstopoResult.length ; i++){
        lstopoResult[i] = lstopoResult[i].split(":")
        if(lstopoResult[i][1] > 1){
            if(lstopoResult[i][0] == "Cache" || lstopoResult[i][0] == "Group") {
                if(lstopoResult[i + 1].includes("NUMANode"))
                    result += lstopoResult[i][1] + "no"
                else
                    result += lstopoResult[i][1] + lstopoResult[i][0].substring(0,2).toLowerCase()
            } else if(lstopoResult[i][0] == "Socket") {
                result += lstopoResult[i][1] + "pa"
            } else {
                result += lstopoResult[i][1] + lstopoResult[i][0].substring(0,2).toLowerCase()
            }
        }
    }
    return result

}
