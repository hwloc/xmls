# hwloc XML database

This database *CANNOT* be used from its dedicated website
(or from the Android app) because of Heroku changes.
For now, it should be browsed manually in the XML/ directory.

Information about each XML is also summarized in the tags.json file.
