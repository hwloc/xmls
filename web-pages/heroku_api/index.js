/*
 * Copyright © 2019-2020 Inria.  All rights reserved.
 */
const express = require("express")
const app = express()
const cors = require('cors')
const testFolder = '../../xml/'
const fs = require('fs')
const path = require('path')

app.use(cors())

app.get("/", (req, res) => {
  let xml = new Array()
  fs.readdir(testFolder, (err, files) => {
    files.forEach(file => {
      xml.push({ title: file })

      if(files.indexOf(file) == files.length - 1)
        res.send({xml})
    })
  })
})

app.get("/tags", async (req, res) => {
    let xmls = new Array()
    fs.readdir(testFolder, (err, files) => {
      files.forEach(file => {
        xmls.push({ title: file })
      })
      let xmls_infos = require(path.join(__dirname, "../../tags.json"))

      let possibleItems = {title: new Array(), tag: new Array()}
      xmls.forEach(xml => {
        xml = xml.title.substring(0, xml.title.length-4)
        possibleItems.title.push(xml)
        
        for(tag in xmls_infos[xml]){

            if(!completion_item_added(possibleItems["tag"], tag))
                possibleItems["tag"].push(tag)
            
            if (possibleItems[tag] == null)
                possibleItems[tag] = new Array()
            
            if(!completion_item_added(possibleItems[tag], xmls_infos[xml][tag]))
                possibleItems[tag].push(xmls_infos[xml][tag])
        }
      })
      json = JSON.stringify(possibleItems)
      json = json.replace(/-/g, "_")
      console.log(json)
      json = JSON.parse(json)
      res.send(json)
    })

    /* Check is an item has already been added to the list of possible tags */
    function completion_item_added(tag, item){
        for(let info of tag){
            if(info == item)
                return true
        }
        return false
    }
})

app.get("/tags/:tags", (req, res) => {
  let req_tags = JSON.parse(req.params.tags)
  let xml = new Array()
  fs.readdir(testFolder,(err, files) => {
    files.forEach(async file => {
      let title = file.substring(0, file.length - 4)
      let jsonfile = "../../tags.json"
      let rawdata = fs.readFileSync(jsonfile)
      let tags = JSON.parse(rawdata)
      let file_tags = tags[title]
    
      let validate = true;
      for(key in req_tags) {
        if(key ==  "title") {
          if(req_tags[key] != null && !file.toLowerCase().includes(req_tags[key].toLowerCase()) ){
            validate = false
            break
          }
            continue
        }

        if(key == "nbcores" || key == "NUMA" || key == "nbpackages" || key == "year"){
          if(req_tags[key] != null && file_tags[key] < req_tags[key]){
            validate = false
            break
          }
            continue
        }

        if(key == "version"){
          if(req_tags[key] != null && file_tags[key] > req_tags[key]){
            validate = false
            break
          }
            continue
        }

        if(typeof req_tags[key] != "string"){
          if(req_tags[key] != null && file_tags[req_tags[key]] == null && file_tags[key] != req_tags[key]){
            validate = false
            break
          }
          continue
        }
        
        if(req_tags[key] != null && file_tags[req_tags[key]] == null && file_tags[key] != req_tags[key]){
          validate = false
          break
        } 
      }
      
      if(validate)
        xml.push({ title: file })
    })
    res.send({xml})
  })
})

app.get("/json", (req, res) => {
  let json = require(path.join(__dirname, "../../tags.json"))
  res.json(json)
})

fs.readdir(testFolder, async (err, files) => {
  files.forEach(file => {
    file = file.replace(/\+/g, '\\+')
    app.get("/xml/" + file, (req, res) => {
      file = file.replace(/\\+/g, '')
      const xml = path.join(__dirname, testFolder + file)
      res.download(xml)
    })
  })
})

const PORT = process.env.PORT || 5000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`)
})