/*
 * Copyright © 2019-2020 Inria.  All rights reserved.
 */
window.onload = async function() {
    const searchBar = document.getElementById("search-bar")
    searchBar.value = "";
    const completion = document.getElementById("completion-list")
    const filters = document.getElementById("filter").children

    let response = await fetch("https://hwloc-xmls.herokuapp.com")
    let xmls = await response.json()
    let response2 = await fetch("https://hwloc-xmls.herokuapp.com/json")
    let xmls_infos = await response2.json()
    completionTags = setCompletionTags(xmls, xmls_infos)

    /*set tag to default value*/
    for(let i = 0 ; i < filters.length ; i++){
        filters[i].firstElementChild.value = "default"
        filters[i].lastElementChild.value = null
    }

    document.getElementById("hwloc-version").value = "default"

    window.addEventListener('keyup',function(event){
        if(event.keyCode === 13){
            searchBar.blur()
            const content = document.getElementById('content')
            deleteChildren(content)
            event.preventDefault()
            search()
        }
    })

    document.getElementById("search-button").addEventListener("click", function(){
        search()
    })
    
    searchBar.addEventListener('input', function(event){
        deleteChildren(completion)
        let searched = searchBar.value

        complete(searched)

        /* if completion is not yet visible and there is something to complete, make it visible */
        if(completion.classList.contains("invisible") && completion.lastElementChild)
            completion.classList.toggle("invisible")
        else if(!completion.classList.contains("invisible") && !completion.lastElementChild)
            completion.classList.toggle("invisible")
    })

    searchBar.addEventListener('focus', function(event){
        deleteChildren(completion)
        complete(searchBar.value)
        if(completion.lastElementChild)
            completion.classList.toggle("invisible")

        if ("createEvent" in document) {
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("hover", false, true);
            completion.firstChild.dispatchEvent(evt);
        }
        else
            completion.firstChild.fireEvent("onhover");
    })

    searchBar.addEventListener('focusout', function(event){
        /* check if the target is a completion item*/
        if(event.explicitOriginalTarget && 
        event.explicitOriginalTarget.parentElement && 
        event.explicitOriginalTarget.parentElement.classList.contains("completion-item") &&
        completion.lastElementChild)
            return
        if(!completion.classList.contains("invisible"))
            completion.classList.add("invisible")
    })

    async function search(){
        let tags = setTags(filters)
        let data = getXMLByTag(xmls, xmls_infos, tags)
        const container = document.getElementById('content')
        /*clear window*/
        deleteChildren(container)
        data.forEach(xml => {
            createXmlElement(xml, container, xmls_infos)
        })
    }

    /* call a first search without parameters to display all xmls */
    search()
}


/**
 * Finds all xml corresponding the search.
 * @param {Object} xmls - Contains all the xml names.
 * @param {Object} xmls_infos - Contains all the topology infos, by name.
 * @param {Object} req_tags - Details of the search.
 */
function getXMLByTag(xmls, xmls_infos, req_tags){
    let xmlReturn = new Array()
    xmls.xml.forEach(xml => {
        xml = xml.title.substring(0, xml.title.length - 4)
        let file_tags = xmls_infos[xml]

        let validate = true;
        for(key in req_tags) {
            if(key ==  "title") {
                if(req_tags[key] != null && !xml.toLowerCase().includes(req_tags[key].toLowerCase()) ){
                    validate = false
                    break
                }
                continue
            }
            if(key == "nbcores" || key == "NUMA" || key == "nbpackages" || key == "year"){
                if(req_tags[key] != null && file_tags[key] < req_tags[key]){
                    validate = false
                    break
                }
                continue
            }
            if(key == "version"){
                if(req_tags[key] != null && file_tags[key] > req_tags[key]){
                    validate = false
                    break
                }
                continue
            }
            if(typeof req_tags[key] != "string"){
                if(req_tags[key] != null && file_tags[req_tags[key]] == null && file_tags[key] != req_tags[key]){
                    validate = false
                    break
                }
            continue
            }
            if(req_tags[key] != null && file_tags[req_tags[key]] == null && file_tags[key].toLowerCase() != req_tags[key].toLowerCase()){
                validate = false
                break
            }
        }

        if(validate)
            xmlReturn.push(xml + ".xml")
    })

    return xmlReturn
}

function deleteChildren(element){
    while(element.firstChild)
        element.removeChild(element.firstChild)
}

function setTags(filters){
    let tags = {}
    let searched
    /* Set version */
    tags.version = document.getElementById("hwloc-version").value == "default" ? null : document.getElementById("hwloc-version").value

    /* Set tags from HTML "select" element */
    for( let i = 0 ; i < filters.length ; i++){
        if(filters[i].firstElementChild.value != "default" && filters[i].lastElementChild.value)
            tags[filters[i].firstElementChild.value] = filters[i].lastElementChild.value
    }

    /* Set tags from search bar */
    searched = document.getElementById('search-bar').value
    tags = parseSearch(searched, tags, completionTags)
    console.log(tags)
    return tags
}

/**
 * Tries to identify what the user is trying to search, if the info is of type "tag:info" write is immediatly, otherwise, call findSearchedTag().
 * @param {String} searched - String to parse, written by the user.
 * @param {Object} tags - Object to return, at the end of the functions, contains all the informations search from the user in the search bar.
 * @param {Object} completionTags - Contains a list of overy possible topology informations.
 */
function parseSearch(searched, tags, completionTags){
    searched = searched.split(';')
    for(let i = 0 ; i < searched.length ; i ++){
        let subsearch = searched[i]
        if(subsearch.includes(':')){
            let keyval = subsearch.split(':')
            tags[keyval[0]] = keyval[1]
        } else {
            tags = findSearchedTag(tags, completionTags, subsearch)
        }
    }
    return tags
}

/**
 * Identify what tag is searched from a string.
 * @param {Object} tags - Object to complete.
 * @param {Object} completionTags - Contains a list of all possible informations.
 * @param {String} searched - String to identify, written by the user.
 */
function findSearchedTag(tags, completionTags, searched){
    for(tag in completionTags){
        if(tag == "title"){
            /*loop through titles*/
            completionTags[tag].forEach(title => {
                if(title.toLowerCase().includes(searched.toLowerCase())){
                    tags[tag] = searched 
                    return
                }    
            })
        }else{
            let expression = findExpression(completionTags[tag], searched)
            if(expression){
                tags[tag] = expression
            }
        }
    }
    return tags
}

function findExpression(list, expr){
    for(let i = 0 ; i < list.length ; i ++){
        if(typeof list[i] == "string") {
            if(list[i].toLowerCase() == (expr.toLowerCase())){
                return list[i]
            }
        } else {
            if(list[i] == expr)
                return list[i]
        }
    }
    return null;
}

/**
 * Create a link to the xml file and a popup window containing the machine's topology.
 * @param {String} xml - Name of the machine.
 * @param {HTML DOM Element Object} container - Container to add the link.
 * @param {Object} xmls_infos - Contains all topology informations, by name.
 */
function createXmlElement(xml, container, xmls_infos){
    let title = xml.substring(0, xml.length-4)
    const span = document.createElement('span')
    const a = document.createElement('a')
    let href = xml
    a.href = "https://hwloc-xmls.herokuapp.com/xml/" + href
    a.innerHTML = xml
    a.classList.add('zoomIn')
    a.classList.add('animated')

    const div = document.createElement("div")
    const p = document.createElement('p')
    const pTitle = document.createElement("p")
    div.classList.add("info-popup", "invisible")
    pTitle.classList.add("title")
    pTitle.innerHTML = title
    for(var key in xmls_infos[title]){
        if(key == "title")
            break
        p.innerHTML += "<strong>" + key + ": </strong>" + xmls_infos[title][key] + "<br>"
    }

    a.addEventListener("mouseover", function(event){
        toggleInvisibleOnPopup(div)
    })
    a.addEventListener("mouseout", function(event){
        toggleInvisibleOnPopup(div)
    })

    span.appendChild(a)
    div.appendChild(pTitle)
    div.appendChild(p)
    container.appendChild(span)
    document.body.appendChild(div)
}

/**
 * Set a list of every possible tags for the completion : return an object containing arrays for each tag types, for instance architecture, vendor, chassis...
 * and an array of tag types
 *  @param {Object} xmls - Contains the name of each xmls
 *  @param {Object} searched - Contains the topology infos of each xmls, by name.
 */ 
function setCompletionTags(xmls, xmls_infos){
    let possibleItems = {title: new Array(), tag: new Array()}

    xmls.xml.forEach(xml => {
        xml = xml.title.substring(0, xml.title.length-4)
        possibleItems.title.push(xml)
        
        for(tag in xmls_infos[xml]){

            if(!completion_item_added(possibleItems["tag"], tag))
                possibleItems["tag"].push(tag)
            
            if (possibleItems[tag] == null)
                possibleItems[tag] = new Array()
            
            if(!completion_item_added(possibleItems[tag], xmls_infos[xml][tag]))
                possibleItems[tag].push(xmls_infos[xml][tag])
        }
    })
    return possibleItems

    /* Check is an item has already been added to the list of possible tags */
    function completion_item_added(tag, item){
        for(let info of tag){
            if(info == item)
                return true
        }
        return false
    }
}

/* Handle the search bar completion */
function complete(searched){
    if(!searched.includes(':')){
        addCompletionItems(completionTags, searched)
    } else {
        completeTag(searched)
    }
}

/**
 * Finds every possible tags matching the data written in the search bar.
 * @param {string} searched - Searched data, written by the user in the search bar
 * @param {string} prefix - Optional parameter, this function is recursive, and split the "searched" parameter before calling itself.
 * "prefix" stores the splitted values, such as : prefix + searched is always equal to what the user wrote in the first place.  
*/
function completeTag(searched, prefix = ""){
    if(searched.includes(';')){
        let tags = searched.split(';')
        let prefix = tags.join("").replace(tags[tags.length - 1], "") + ";"
        completeTag(tags[tags.length - 1], prefix)
    } else if(!searched.includes(':')){
        createCompletionItemsByTag("tag", searched, prefix)
    } else {
        let tag = searched.split(':')
        prefix = prefix + tag[0] + ":"
        createCompletionItemsByTag(tag[0], tag[1], prefix)
    }
}

function addCompletionItems(completionTags, searched){
    for(tag in completionTags){
        createCompletionItemsByTag(tag, searched)
    }
}

function createCompletionItemsByTag(tag, searched, prefix = false){
    completionTags[tag].forEach(item => {
        if(typeof item == "string" && item.toLowerCase().includes(searched.toLowerCase()))     
            createCompletionItem(item, prefix)
    })
}

/**
 * Create the clickable element to complete the search.
 * @param {string} item - String to add to the prefix.
 * @param {string} prefix - Searched data, makes sure to add the completion to the string and not replace it.
 */ 
function createCompletionItem(item, prefix){
    const completion = document.getElementById('completion-list')
    const searchBar = document.getElementById('search-bar')

    let completion_item = document.createElement('p')
    completion_item.innerHTML = item
    completion_item.classList.add("completion-item")
    completion_item.addEventListener('click', function(event){
        if(prefix)
            searchBar.value = prefix + item
        else
            searchBar.value = event.target.innerHTML
        completion.classList.toggle("invisible")
        searchBar.focus()
    })
    completion.appendChild(completion_item)
}

function toggleInvisibleOnPopup(element){
    element.classList.toggle('invisible')
}


/* Handle the HTML "select" elements and their possible values */
function setNextVisible(event){
    const select = event.target
    if(select.oldValue)
        addOption(select.oldValue, select.parentElement.id)
    if(select.parentElement.id < 4)
        document.getElementById(parseInt(select.parentElement.id) + 1).classList.remove("invisible")
    
    deleteOption(select.options[select.selectedIndex].value, select.parentElement.id)
    select.oldValue = select.options[select.selectedIndex]

    function deleteOption(selected, id){
        toDeleteOptions = document.querySelectorAll("div#filter option[value=\"" + selected  + "\"]")
        for( let i = 0; i < 4 ; i++ ){
            if((i + 1 ) == id)
                continue
            toDeleteOptions[i].remove()
        }
    }
    
    function addOption(selected, id){
        for( let i = 1 ; i <= 4 ; i++ ){
            selected = selected.cloneNode(true)
            if(i == id)
                continue
            let container = document.getElementById(i)
            container.firstElementChild.appendChild(selected)
        }
    }
}


