#!/usr/bin/node
/*
 * Copyright © 2019-2020 Inria.  All rights reserved.
 */
const fs = require('fs')
const xml2js = require('xml2js')

let xmldir
let tagfile

if (fs.existsSync("../xml") && fs.existsSync("../tags.json")) {
    xmldir = "../xml"
    tagfile = "../tags.json"
} else if (fs.existsSync("./xml") && fs.existsSync("./tags.json")) {
    xmldir = "./xml"
    tagfile = "./tags.json"
} else
    throw new Error("Couldn't find xml directory or tags.json file.")

fs.readdir(xmldir,(err, files) => {
    let rawdata = fs.readFileSync(tagfile)
    let json = JSON.parse(rawdata)
    let json_size = Object.keys(json).length
    let missingXmls = ""

    missingXmls = searchMissingInfoInTags(json, json_size, files, missingXmls)
    missingXmls = searchMissingInfoInXmls(json, json_size, files, missingXmls)

    if(missingXmls == "")
        console.log("\n OK ! \n")
    else
        console.log(missingXmls)
})

function searchMissingInfoInTags(json, json_size, files, missingXmls){
    files.forEach(file => {
        file = file.substring(0, file.length - 4)
        if(!json[file] && !json[file + ".xml"]){
            missingXmls += "\n Missing " + file + " in " + tagfile + " \n"
        }
    })
    return missingXmls
}

function searchMissingInfoInXmls(json, json_size, files, missingXmls){
    let extension
    for(key in json){
        extension = ""
        if(!key.includes(".xml"))
            extension = ".xml"
        if(!files.includes(key + extension))
            missingXmls += "\n Missing " + key + extension + " in " + xmldir + " \n"
    }
    return missingXmls
}
